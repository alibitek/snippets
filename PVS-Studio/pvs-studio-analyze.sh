#!/usr/bin/env bash

# find src/ -type f -name *.c -exec head -n 4 {} \; | grep -c 'This is an independent project'

set -x -o pipefail

SOURCE_PATH=$(pwd -L)/src

echo "Add free usage comments"
# https://github.com/viva64/how-to-use-pvs-studio-free
how-to-use-pvs-studio-free -c 3 -m "$SOURCE_PATH"

echo "Cleanup"
make clean

echo "Trace build tool to gather compiler flags"
rm strace_out
pvs-studio-analyzer trace -- make all -j$(nproc)

cat strace_out

echo "Launch PVS-Studio"

# Analysis mode defines the type of warnings
# 1 - 64-bit errors;
# 4 - General Analysis;
# 8 - Micro-optimizations;
# Modes can be combined by adding the values
# Default: 4
        
rm PVS-Studio.log

# PVS-Studio-Free.lic
pvs-studio-analyzer analyze -l PVS-Studio-Trial.lic -v -a 13 -o PVS-Studio.log
cat PVS-Studio.log

rm PVS_Studio_results.txt
plog-converter -a 'GA;64;OP' -t tasklist -o PVS_Studio_results.txt PVS-Studio.log
cat PVS_Studio_results.txt

How to build rsync? 
1. `git clone git://git.samba.org/rsync.git`
2. `chmod +x ./build_rsync.sh && ./build_rsync.sh`

```
#!/usr/bin/env bash

./configure --prefix=/usr \
		--with-included-popt=yes \
		--with-included-zlib=yes \
		--disable-debug
		
make -j$(nproc)
```

#!/usr/bin/env bash

# Find out the list of all the Linux kernel modules needed to control the hardware in the machine

for i in `find /sys/ -name modalias -exec cat {} \;`; do
    modprobe --config /dev/null --show-depends $i 2>/dev/null;
done | rev | cut -f 1 -d '/' | rev | sort -u

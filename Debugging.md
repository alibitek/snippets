Tehnica simpla de (software) debugging:

De multe ori, debugging-ul se rezuma la a face multe teste, in diferite contexte. Asa iti poti da seama de problema.

Cum fac eu asta, mai exact? Pornind de la ideea ca am un caz nefunctional, iau un exemplu similar, care functioneaza, si incerc sa-l duc, treptat, pana in punctul in care devine identitc cu cazul meu, nefunctional. Cand zic treptat ma refer ca trecerea sa fie facuta in pasi marunti, iar  fiecare pas - testat.

Daca urmezi acest parcurs, e imposibil sa nu te intalnesti cu bug-ul, pe drum. Vei sti exact la ce pas s-a produs ruptura.

Exemplu: Primim “Call to undefined function...” cand noi avem impresia ca am definit functia cum/unde trebuia. In acest caz ar presupune ca, pornind de la un exemplu similar si functional, sa schimbam locul unde este definita functia, sa schimbam numele ei, sa schimbam locul unde ea este apelata… iar la fiecare dintre acesti pasi, sa testam. 

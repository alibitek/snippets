#!/bin/sh

# Credits: https://web.archive.org/web/20120130171635/http://www.intuitivenipple.net/10/converting-mp3s-to-m4b-audiobooks-and-m4b-to-mp3

IFS=$'\n'

for m4b in $(ls -1 *.m4b); do ffmpeg -i "$m4b" -acodec libmp3lame -ar 22050 "${m4b}.mp3"; done

unset IFS

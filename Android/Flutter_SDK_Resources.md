# Flutter

## [Widgets](https://flutter.io/widgets/)
- [Material Design Widgets](https://flutter.io/widgets/material/)
- [Cupertino (iOS-style) Widgets ](https://flutter.io/widgets/cupertino/)

## [Codelabs](https://codelabs.developers.google.com/codelabs/flutter)
- [Building Beautiful UIs with Flutter](https://codelabs.developers.google.com/codelabs/flutter/index.html)
- [Firebase for Flutter](https://codelabs.developers.google.com/codelabs/flutter-firebase/index.html)

## Icons
- [Add custom font-icons to a flutter project](https://medium.com/@emv_tech/add-custom-font-icons-to-a-flutter-project-12bddb841d7)

## Advocacy
### Articles
- 2017
    - November
        - [November 13 2017 - ‘Cross Platform’ Hack Day: Google Flutter](https://medium.com/@rantmediaUK/cross-platform-hack-day-google-flutter-7c1b731cd6bb)
        - [November 16 2017 - New Beginnings: Joining Google](https://medium.com/@timsneath/new-beginnings-google-bf849766a497)
        - [November 16 2017 - Why Flutter doesn’t use OEM widgets](https://medium.com/flutter-io/why-flutter-doesnt-use-oem-widgets-94746e812510)
        - [November 21 2017 - Why Flutter? and not framework X? or better yet why i’m Going Flutter all in.](https://medium.com/@franzsilva/why-flutter-and-not-framework-x-or-better-yet-why-im-going-flutter-all-in-b484ecb25336)
        - [November 22 2017 - First look at Flutter](https://medium.com/@jeff.smith.au/first-look-at-flutter-b7bf891a605)
        - [November 25 2017 - Cross-Platform Mobile App Development with Flutter — Xamarin — React Native: A Performance Focused Comparison](https://medium.com/@korhanbircan/cross-platform-mobile-app-development-with-flutter-xamarin-react-native-a-performance-focused-a4457bcbdacc)
            - [Slides](https://www.slideshare.net/KorhanBircan/crossplatform-app-development-with-flutter-xamarin-react-native)
        - [November 27 2017 - Why Flutter Will Take Off in 2018](https://codeburst.io/why-flutter-will-take-off-in-2018-bbd75f8741b0) 
        - [November 30 2017 - Work with Flutter basic widget](https://medium.com/@essayerit/work-with-flutter-basic-widget-12ac1a8b7a24)
        - [November 30 2017 - Flutter — A Game Changer In The Realm Of Mobile Solutions?](https://blog.hackerbay.io/flutter-a-game-changer-in-the-realm-of-mobile-solutions-5672f28f62b7)

### Videos
- 2016
    - [October 31 2016 - AOT compiling Dart for iOS Android (Dart Developer Summit 2016)](https://www.youtube.com/watch?v=lqE4u8s8Iik)
- 2017
    - [May 9 2017 - Droidcon Italy 2017: A new hope - Eugenio Marletti & Sebastiano Poggi](https://www.youtube.com/watch?v=0ijVuVtu6a4)
    - [June 20 2017 - FLOSS Weekly 439 Flutter](https://twit.tv/shows/floss-weekly/episodes/439)
    - October
        - [October 25 2017 - ReactiveConf 2017: Architecting the Reactive App with Flutter](https://www.youtube.com/watch?v=n_5JULTrstU)
        - [October 25 2017 - ReactiveConf 2017: Gavin Doughtie AMA](https://www.youtube.com/watch?v=F28ecHrGH3w)
    - December
        - [December 15 2017 - What is Flutter and why should I care by Sergi Martinez - DevFestHH17](https://www.youtube.com/watch?v=Nm_mdptybf0)  
    
## Apps
- [Flutter gallery](https://github.com/flutter/flutter/tree/master/examples/flutter_gallery)
- [Pesto]()
- [Stocks app](https://github.com/flutter/flutter/tree/master/examples/stocks)
- [Google Inbox Clone](https://github.com/franzsilva/FlutterInboxClone)
- [Google Messages App Clone](https://github.com/franzsilva/FlutterMessagesClone)
- [T Ami](https://play.google.com/store/apps/details?id=com.tamiflutter)
- Hamilton
    - [It must be nice to have Hamilton on your phone](https://www.blog.google/topics/developers/it-must-be-nice-have-hamilton-your-phone/)
    - [Hamilton - The Official App - iOS](https://itunes.apple.com/us/app/hamilton-the-official-app/id1255231054?mt=8)
    - [Hamilton - The Official App - Android](https://play.google.com/store/apps/details?id=com.hamilton.app&hl=en)
    - [HAMILTON App Gets Over 500K Downloads in First Three Days, Features Cutting Edge Tech](https://www.broadwayworld.com/article/HAMILTON-App-Gets-Over-500K-Downloads-in-First-Three-Days-Features-Cutting-Edge-Tech-20170814)
- [Greentea - a CRM app at Google](https://www.youtube.com/watch?v=IMNUiC2O9M8)
- Google AdSense
    - [Google gives its AdSense website a fresh Material Design overhaul](https://9to5google.com/2016/10/12/google-gives-its-adsense-website-a-facelift-with-a-fresh-material-design-coating/)
    - [First Look: The New AdWords Experience](https://www.phillymarketinglabs.com/blog/first-look-the-new-adwords-experience/)
    - [The New Google Merchant Center: What You Need to Know](https://www.business2community.com/online-marketing/new-google-merchant-center-need-know-01648550)
- Google AdWords
- Google Fiber
- Soundtrap (swedish startup)
